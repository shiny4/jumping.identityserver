﻿using IdentityServer4.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace Jumping.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<ApiResource> GetApiResources() => new List<ApiResource>
        {
            new ApiResource("ProfileService","Profile API"){Scopes = { "SharedScope" } },
            new ApiResource("LessonsService", "Lessons API"){Scopes = { "SharedScope" } },
            new ApiResource("ApplicationService", "Application API"){Scopes = { "SharedScope" } },
            new ApiResource("NotificationService", " Notification API"){Scopes = { "SharedScope" } }
        };

        public static IEnumerable<ApiScope> GetScopes() => new List<ApiScope>
        {
            new ApiScope("SharedScope")
        };

        public static IEnumerable<Client> GetClients() => new List<Client>
        {
            new Client
            {
                ClientId = "madi.abdigaliyev@gmail.com",
                AllowedGrantTypes = GrantTypes.ClientCredentials,

                Claims = new List<ClientClaim>
                {
                    new ClientClaim(ClaimTypes.Role,"Student"),
                    new ClientClaim(ClaimTypes.Sid, "9823de36-488a-4a0d-8402-90bac39e4999"),
                    new ClientClaim(ClaimTypes.Email, "madi.abdigaliyev@gmail.com"),
                },

                ClientSecrets =
                {
                    new Secret("secret".Sha256())
                },

                AllowedScopes = { "SharedScope" }
            },
             new Client
            {
                ClientId = "admin.admin@gmail.com",
                AllowedGrantTypes = GrantTypes.ClientCredentials,

                Claims = new List<ClientClaim>
                {
                    new ClientClaim(ClaimTypes.Role,"Admin"),
                    new ClientClaim(ClaimTypes.Sid, "22b26554-6e22-4aa1-a8be-fb5a8af8615e"),
                    new ClientClaim(ClaimTypes.Email, "admin.admin@mail.ru")
                },

                ClientSecrets =
                {
                    new Secret("secret".Sha256())
                },

                AllowedScopes = { "SharedScope" }
            }
        };
    }
}
