﻿using Microsoft.AspNetCore.Identity;

namespace Jumping.IdentityServer.Models
{
    public class ApplicationUser : IdentityUser
    {
        public Role Role { get; set; }
    }
}
