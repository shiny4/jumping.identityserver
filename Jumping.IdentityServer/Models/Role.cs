﻿namespace Jumping.IdentityServer.Models
{
    public enum Role
    {
        Student,
        Teacher,
        Both,
        Admin
    }
}
