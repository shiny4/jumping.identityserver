﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace Jumping.IdentityServer
{
    public class IdentityServerConfigOptions
    {
        public IEnumerable<ApiScope> ApiScopes { get; set; }
        public string Admin { get; set; }
        public string AdminSecret { get; set; }
        public string ConnectionString { get; set; }
    }
}
