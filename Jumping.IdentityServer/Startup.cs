using Jumping.IdentityServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure;
using System;
using System.Reflection;

namespace Jumping.IdentityServer
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private string _migrationsAssembly;
        private IdentityDatabase _identityDatabase;
        public Startup(IConfiguration configuration)
        {
            _identityDatabase = new IdentityDatabase();
            Configuration = configuration;
            _migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var identityServerConfig = Configuration
                .GetSection("IdentityServerConfiguration")
                .Get<IdentityServerConfigOptions>();

            services.AddIdentityServer()
                  .AddConfigurationStore(options =>
                  {
                      options.ConfigureDbContext = b => b
                        .UseNpgsql(identityServerConfig.ConnectionString, 
                        sql => sql.MigrationsAssembly(_migrationsAssembly));
                  })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b
                        .UseNpgsql(identityServerConfig.ConnectionString, sql => sql.MigrationsAssembly(_migrationsAssembly));
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30;

                }).AddDeveloperSigningCredential();


            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            _identityDatabase.InitializeDatabase(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseIdentityServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
