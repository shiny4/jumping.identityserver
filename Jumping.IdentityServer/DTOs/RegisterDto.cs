﻿using Jumping.IdentityServer.Models;
using System;

namespace Jumping.IdentityServer.DTOs
{
    public class RegisterDto
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
    }
}
