﻿using Jumping.IdentityServer.DTOs;
using Jumping.IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jumping.IdentityServer.Controllers
{
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        
        [HttpPost]
        public async Task<IActionResult> RegisterAsync(RegisterDto registerDto)
        {
            var user = new ApplicationUser()
            {
                Id = registerDto.UserId.ToString(),                
                Email = registerDto.Email,
                Role = registerDto.Role,               
            };
            await _userManager.CreateAsync(user, registerDto.Password);
            return Ok();
        }
    }
}
